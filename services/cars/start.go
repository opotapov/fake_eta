package main

import (
    "fmt"
    "log"
	"net/http"
	"strconv"
	"math"
	"sort"
	"encoding/json"
)

type Car struct {
	Id int64 `json:"id"`
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

func healthHandler(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Status=OK")
}

func carsHandler(w http.ResponseWriter, r *http.Request) {

	lat_str, ok := r.URL.Query()["lat"]
    if !ok || len(lat_str[0]) < 1 {
		http.Error(w, "Url Param 'lat' is missing", http.StatusBadRequest)
        return
	}
	
	lng_str, ok := r.URL.Query()["lng"]
    if !ok || len(lng_str[0]) < 1 {
		http.Error(w, "Url Param 'lng' is missing", http.StatusBadRequest)
        return
	}
	
	limit_str, ok := r.URL.Query()["limit"]
    if !ok || len(limit_str[0]) < 1 {
		http.Error(w, "Url Param 'limit' is missing", http.StatusBadRequest)
        return
    }

	lat, _ := strconv.ParseFloat(lat_str[0], 64)
	lng, _ := strconv.ParseFloat(lng_str[0], 64)
	limit, _ := strconv.ParseInt(limit_str[0], 0, 64)

	result := getCars(lat, lng, limit)

	w.Header().Set("Content-Type", "application/json")
    json.NewEncoder(w).Encode(result)
}

func main() {
	http.HandleFunc("/fake-eta/_health.json", healthHandler)
	http.HandleFunc("/fake-eta/cars.json", carsHandler)
	
    log.Fatal(http.ListenAndServe(":8081", nil))
}

func getCars(lat float64, lng float64, limit int64) []Car {
	cars := getCarsFromRepository()

	return getNearestCars(cars, lat, lng, limit)
}

func getCarsFromRepository() []Car {
	return []Car{
		Car{ 1, 40.68433, -74.39967 },
		Car{ 2, 50.68433, 74.39967 },
		Car{ 3, 55.7575429, 37.6135117 },
		Car{ 4, 55.74837156167371, 37.61180107665421 },
		Car{ 5, 55.7532706, 37.6076902 },
		Car{ 6, 55.8532706, 37.7076902 },
		Car{ 7, 55.9532706, 37.8076902 },
		Car{ 8, 56.7532706, 37.9076902 },
		Car{ 9, 57.7532706, 37.6076902 },
		Car{ 10, 58.7532706, 38.6076902 },
		Car{ 11, 58.8532706, 39.6076902 },
		Car{ 12, 58.9532706, 39.7076902 },
	}
}

func getNearestCars(cars []Car, lat float64, lng float64, limit int64) []Car {
	var cars_map = make(map[int64]Car)

	for _, car := range cars {
		cars_map[car.Id] = car
	}

	var distances = make(map[float64]int64)
	for _, car := range cars {
		lat_diff := car.Lat - lat
		lng_diff := car.Lng - lng

		distance := math.Sqrt(lat_diff*lat_diff + lng_diff*lng_diff)
		distances[distance] = car.Id
	}

	var distance_values = make([]float64, 0, len(distances))
	for k := range distances {
		distance_values = append(distance_values, k)
	}
	sort.Float64s(distance_values)

	var result = []Car{}
	for i := int64(0); i < limit && i < int64(len(distance_values)); i++ {
		car_id := distances[distance_values[i]]

		result = append(result, cars_map[car_id])
	}
	
	return result
}
