package main

import "testing"

func TestGetNearestCarsBasic(t *testing.T) {
    cars := []Car{
		Car{ 1, 40.68433, -74.39967 },
		Car{ 2, 50.68433, 74.39967 },
	}
    result := getNearestCars(cars, 5.123, 5.13, 1)

    if result[0].id != 2 {
       t.Errorf("Sort is not correct, first element should be %d, got %d", 2, result[0].id)
    }

    if len(result) != 1 {
        t.Errorf("Limit is not correct, length should be %d, got %d", 1, len(result))
     }
}

func TestGetNearestCarsLimit(t *testing.T) {
    cars := []Car{
		Car{ 1, 40.68433, -74.39967 },
		Car{ 2, 50.68433, 74.39967 },
    }
    
    result1 := getNearestCars(cars, 5.123, 5.13, 2)
    if len(result1) != 2 {
        t.Errorf("Limit is not correct, length should be %d, got %d", 2, len(result1))
    }

    result2 := getNearestCars(cars, 5.123, 5.13, 5)
    if len(result2) != 2 {
        t.Errorf("Limit is not correct, length should be %d, got %d", 2, len(result2))
    }
}
