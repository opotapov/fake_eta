package main

import (
    "fmt"
    "log"
	"net/http"
	"math"
	"encoding/json"
)

type Coord struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

type RequestStruct struct {
	Target Coord `json:"target"`
	Source [] Coord `json:"source"`
}

func healthHandler(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Status=OK")
}

func predictHandler(w http.ResponseWriter, r *http.Request) {
	var reqStruct RequestStruct
	err := json.NewDecoder(r.Body).Decode(&reqStruct)

	if err != nil {
		http.Error(w, "Request body can't be parsed", http.StatusBadRequest)
        return
	}

	result := getPredictions(reqStruct.Target, reqStruct.Source)

	w.Header().Set("Content-Type", "application/json")
    json.NewEncoder(w).Encode(result)
}

func main() {
	http.HandleFunc("/fake-eta/_health.json", healthHandler)
	http.HandleFunc("/fake-eta/predict.json", predictHandler)
	
    log.Fatal(http.ListenAndServe(":8083", nil))
}

func getPredictions(target Coord, source []Coord) []int64{
	var result = []int64{}

	for _, source_item := range source {
		lat_diff := source_item.Lat - target.Lat
		lng_diff := source_item.Lng - target.Lng

		distance := math.Sqrt(lat_diff*lat_diff + lng_diff*lng_diff)

		result = append(result, getTimeFromDistance(distance))
	}

	return result
}

func getTimeFromDistance(distance float64) int64{
	return int64(distance * 100) + 2
}