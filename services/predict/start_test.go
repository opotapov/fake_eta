package main

import "testing"
import "reflect"

func TestGetTimeFromDistance(t *testing.T) {
    result := getTimeFromDistance(0.01234)

    if result != 3 {
       t.Errorf("Time calc is not correct, should be %d, got %d", 3, result)
    }
}

func TestGetPredictions(t *testing.T) {
    target := Coord{ 55.7532706, 37.6076902 }

    source := []Coord{
        Coord{ 55.7632706, 37.6076902 },
        Coord{ 55.7732706, 37.6076902 },
    }

    result := getPredictions(target, source)

    if !reflect.DeepEqual(result, []int64{2, 3}) {
        t.Errorf("Result is not correct, should be %v, got %v", []int64{2, 3}, result)
    }
}