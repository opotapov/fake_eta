package main

import "testing"

func TestGetMinTime(t *testing.T) {
    result := getMinTime([]int64{2, 3, 4, 1})

    if result != int64(1) {
       t.Errorf("Result is not correct, should be %d, got %d", 1, result)
    }
}