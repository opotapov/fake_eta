package main

import (
    "fmt"
    "log"
	"net/http"
	"strconv"
	"bytes"
	"encoding/json"
)

const PredictionServiceUrl = "http://fake_eta_predict_haproxy_1:80/fake-eta/predict.json"
const CarsServiceUrl = "http://fake_eta_cars_haproxy_1:80/fake-eta/cars.json"
const CarsLimit = 5

type Coord struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

type RequestStruct struct {
	Target Coord `json:"target"`
	Source [] Coord `json:"source"`
}

type Car struct {
	Id int64 `json:"id"`
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

func healthHandler(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Status=OK")
}

func timeHandler(w http.ResponseWriter, r *http.Request) {
	lat_str, ok := r.URL.Query()["lat"]
    if !ok || len(lat_str[0]) < 1 {
		http.Error(w, "Url Param 'lat' is missing", http.StatusBadRequest)
        return
	}
	
	lng_str, ok := r.URL.Query()["lng"]
    if !ok || len(lng_str[0]) < 1 {
		http.Error(w, "Url Param 'lng' is missing", http.StatusBadRequest)
        return
	}

	lat, _ := strconv.ParseFloat(lat_str[0], 64)
	lng, _ := strconv.ParseFloat(lng_str[0], 64)

	car_resp, car_err := http.Get(CarsServiceUrl + fmt.Sprintf("?lat=%f&lng=%f&limit=%d", lat, lng, CarsLimit))
	
	if car_err != nil {
		http.Error(w, "Can't get cars", http.StatusBadRequest)
        return
	}

	var cars []Car
	json.NewDecoder(car_resp.Body).Decode(&cars)

	var sources []Coord
	for _, car := range cars {
		sources = append(sources, Coord{car.Lat, car.Lng})
	}

	reqStruct := RequestStruct{Coord{lat, lng}, sources}
    reqStructJson, _ := json.Marshal(reqStruct)
    u := bytes.NewReader(reqStructJson)

	pred_resp, pred_err := http.Post(PredictionServiceUrl, "application/json", u)

	if pred_err != nil {
		http.Error(w, "Can't get time", http.StatusBadRequest)
        return
	}

	var times []int64
	json.NewDecoder(pred_resp.Body).Decode(&times)

	w.Header().Set("Content-Type", "application/json")
    json.NewEncoder(w).Encode(getMinTime(times))
}

func main() {
	http.HandleFunc("/fake-eta/_health.json", healthHandler)
	http.HandleFunc("/fake-eta/time.json", timeHandler)
	
    log.Fatal(http.ListenAndServe(":8080", nil))
}

func getMinTime(times []int64) int64 {
	min := times[0]
	for _, value := range times {
		if value < min {
			min = value
		}
	}
	return min
}